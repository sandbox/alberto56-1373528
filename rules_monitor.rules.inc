<?php
/**
 * @file
 * Implements "rules" (drupal.org/project/rules) hooks.
 */

/*
 * Implements hook_rules_event_info().
 *
 * Defines the condition "Minimum interval of one day", meant to
 * be used with the Cron Run event, and designed to limit the
 * frequency of the triggered actions.
 */
function rules_monitor_rules_condition_info() {
  $conditions = array();
  $conditions['rules_monitor_condition_min_interval'] = array(
    'label' => t('Minimum interval of one day'),
    'group' => t('Rules Monitor'),
  );
  $conditions['rules_monitor_site_status_dirty'] = array(
    'label' => t('Site status contains errors'),
    'group' => t('Rules Monitor'),
  );
  return $conditions;
}
