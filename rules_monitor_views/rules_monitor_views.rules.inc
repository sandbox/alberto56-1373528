<?php

/**
 * @file
 * Rules hooks. This file only contains the conditions hook, defining a
 * condition (which is also a state of the site). This is meant to be used
 * as follows:
 * (1) create a new rule set to fire on the event "cron job"
 * (2) one of the conditions will be that a given view be populated. That
 *     is the condition defined here.
 * (3) To avoid getting emails at every cron job, you will also set the
 *     condition, defined in rules_monitor module, that a certain time
 *     has elapsed.
 */

/**
 * Implements hook_rules_condition_info().
 *
 * Declares the possible state where a given view has at least one row. See
 * README.txt for how to use this.
 */

function rules_monitor_views_rules_condition_info() {
  $conditions = array();

  $conditions['rules_monitor_views_condition_populated_view'] = array(
    'label' => t('View is not empty'),
    'group' => t('Rules Monitor'),
    'parameter' => array(
      'view_name' => array(
        'type' => 'text',
        'label' => t('View to check'),
        'options list' => 'rules_monitor_views_views_list',
      ),
    ),
  );

  return $conditions;
}
