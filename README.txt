Rules Monitor (rules_monitor)

Documentation
-------------

- This README.txt file contains basic instructions. It is considered canonical.
- The book page at http://drupal.org/node/1387366 contains information from the
  README.txt file. In case of discrepancy between the book page and this
  README.txt file, the latter is considered canonical.
- The project home page http://drupal.org/sandbox/alberto56/1373528 contains
  links to the documentation, news, issues, the roadmap, and other resources.

Description
-----------

Allows you to use Rules (drupal.org/project/rules) to check your site's state every day, and fire an action if a state is TRUE, but never more than once daily. Consider the following example.

Let's say you want to email all users of the role "comment moderator" if there are one or more unmoderated comments on your site; and you want this to happen once per day.

(1) download rules_monitor and views as you would any other module. Enable
    rules_monitor_views, views_ui, rules_admin (Rules UI) and dependencies;
(2) (admin/people/permissions) configure permissions if you are not user 1;
(3) (admin/people/permissions/roles) make sure you have a role named "comment
    moderator" on your site;
(4) create a view which displays unmoderated comments, making it accessible
    to trusted users only (Using views is outside the scope of this document);
(5) go to config > workflow > rules.
(6) create a rule to be triggered on the event "Cron maintenance tasks are
    performed".
(7) select the condition: "Rules Monitor: View is not empty", and select, as a
    parameter, the name of the view you created.
(8) select a second condition: "Rules Monitor: Minimum interval of one day".
(9) set the action to "Send mail to all users of a role". As parameters, select
    the role "comment moderator", set the email text and subject, and save.

Now, daily (or on cron runs, whichever is least frequent), any user with the role "comment moderator" will receive an email only if the "unmoderated comments" view is not empty.

Note that you do *not* want to notify users *every time* there is a new comment -- That's not the point of this module -- but rather to check your site daily to see if there are one or more unmoderated comments. Concretely, this means that the following scenario is possible:

(1) Noon on Jan. 1st: site is checked for unmoderated comments, there are none.
(2) 6 p.m. on Jan. 1st: a new comment is posted.
(3) 7 p.m. a comment moderator processes the new comment, either publishing
    or deleting it.
(4) Noon on Jan. 2nd: site is checked for unmoderated comments, there are none.

In the above scenario, the action associated with the Rules Monitor rule is not called.

Inner workings: overview
------------------------

Rules works by reacting to events, checking for conditions and firing an action if the conditions are TRUE. In the example above, the event is the cron job, and the conditions are that a view not be empty, and that at least a day has passed since this was last checked.

Thus, all Rules Monitor really does is to define those conditions.

For Developers: Extending Rules Monitor
---------------------------------------

- Get familiar with the Rules API and online documentation.
- Site states are defined in exactly the same way as rules conditions.
- To learn by example, look at the included rules_monitor_views module, and run the included Simpletests and look at the resulting verbose messages.

Automated testing
-----------------

This project uses Test-Driven Development: all features are validated by tests.

Timing & intervals
------------------

As of this writing, all states are checked daily or on cron runs, whichever is less often. Eventually, more fine-grained per-event intervals could be defined (see http://drupal.org/node/1387248).

To change the interval you can modify the variable (programmatically, through drush or through devel) "rules_monitor_maxtime" and set it to the number of seconds you want. For example, for three days, use "259200" (3 days * 24 hours * 60 minutes * 60 seconds). This technique is neither tested nor supported until http://drupal.org/node/1387248 is fixed.

Branches
--------

7.x-1.x-dev: No failing tests will be pushed here. This is the branch you should use.
MASTER: The MASTER branch is undefined and should not be used.

Version history
---------------

  7.x-1.x, 2012-01-01
  -------------------
  - #1376546 states should be conditions, not events -- cron is the event
  - #1376636 Set project name to Rules Monitor
  - #1376678 remove sleep() function from test file
  - #1379480 Remove devel as dependency in tests

  7.x-1.x, 2011-12-17
  ------------------
  - Development started

License
-------

See enclosed LICENSE.txt file

Author
------

Albert Albala (Look for user alberto56 on Drupal.org)